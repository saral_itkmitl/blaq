<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('wallet_id',10)->nullable();
            $table->string('firstName',50)->nullable();
            $table->string('surName',50)->nullable();
            $table->string('phone',50)->nullable();
            $table->string('sso_id')->nullable();
            $table->string('email',50)->unique();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
        

    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
