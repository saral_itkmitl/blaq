<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([
  		'itemName' => 'Singha Beer(Free)',
  		'description' => 'All Stages',
        'price' => '120',
        'stock' => '1000',
        'limit' => '1000',
        'item_path' => 'img/01.jpg',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
         DB::table('items')->insert([
  		'itemName' => 'Singha Beer',
  		'description' => 'All Stages',
        'price' => '60',
        'stock' => '1000',
        'limit' => '1000',
        'item_path' => 'img/01.jpg',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('items')->insert([
  		'itemName' => 'Moose Cider',
  		'description' => 'All Stages',
        'price' => '80',
        'stock' => '1000',
        'limit' => '1000',
        'item_path' => 'img/02-1.jpg',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('items')->insert([
  		'itemName' => 'Popcorn',
  		'description' => 'Blaq Lyte Stage',
        'price' => '50',
        'stock' => '1000',
        'limit' => '1000',
        'item_path' => 'img/03.jpg',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('items')->insert([
  		'itemName' => 'Fowl Munchies',
  		'description' => 'Blaq Lyte Stage',
        'price' => '150',
        'stock' => '1000',
        'limit' => '1000',
        'item_path' => 'img/06.jpg',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('items')->insert([
  		'itemName' => 'Crispy Pork 100g',
  		'description' => 'Blaq Lyte Stage',
        'price' => '55',
        'stock' => '1000',
        'limit' => '1000',
        'item_path' => 'img/03-1.jpg',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
         DB::table('items')->insert([
  		'itemName' => 'Focaccia Chicken Sandwich',
  		'description' => 'Future Factory Stage',
        'price' => '100',
        'stock' => '1000',
        'limit' => '1000',
        'item_path' => 'img/02.jpg',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('items')->insert([
  		'itemName' => "Jong's Fries",
  		'description' => 'Future Factory Stage',
        'price' => '180',
        'stock' => '1000',
        'limit' => '1000',
        'item_path' => 'img/07.jpg',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
    
        DB::table('items')->insert([
  		'itemName' => 'Powerballs (6 pcs)',
  		'description' => 'Future Factory Stage',
        'price' => '120',
        'stock' => '1000',
        'limit' => '1000',
        'item_path' => 'img/08.png',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('items')->insert([
  		'itemName' => 'Ice Hojicha',
  		'description' => 'Future Factory Stage',
        'price' => '150',
        'stock' => '1000',
        'limit' => '1000',
        'item_path' => 'img/09.jpg',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('items')->insert([
  		'itemName' => 'Smoked Pork Hotdog w/ Melted Cheese',
  		'description' => 'Future Factory Stage',
        'price' => '150',
        'stock' => '1000',
        'limit' => '1000',
        'item_path' => 'img/10.jpg',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('items')->insert([
  		'itemName' => 'SangSom Mix',
  		'description' => 'Auntys Haus Stage',
        'price' => '80',
        'stock' => '1000',
        'limit' => '1000',
        'item_path' => 'img/04.png',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('items')->insert([
  		'itemName' => 'Chicken Wing',
  		'description' => 'Auntys Haus Stage',
        'price' => '80',
        'stock' => '1000',
        'limit' => '1000',
        'item_path' => 'img/05.png',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        
        
        
        
        DB::table('codes')->insert([
  		'code' => '486468',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '687672',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '348752',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '876786',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '637862',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '546456',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '873542',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '453512',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '876125',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '876876',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '964524',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '445272',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '424388',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '868435',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '563563',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '453453',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '985515',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
        
        DB::table('codes')->insert([
  		'code' => '753851',
  		'status' => 'ready',
  		'created_at' => date('Y-m-d H:i:s')
  		]);
    }
}
