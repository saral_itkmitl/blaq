<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wallet;
use App\User;
use App\Item;
use App\Transaction;
use App\Code;
class DashboardController extends Controller
{
    public function search()
    {
        $not = '0';
         return view('not-found');
    }


    public function staff($code)
    {
        $tran = Transaction::where('code', '=', $tran->$code->code)->first();
        if(!empty($status)){
             return view('search')->withTran($tran);
        }else{
            $not = '1';
            return view('not-found');
        }
        
    }

       public function dashboard()
    {
 
           
        $item = Item::all();

           
        $login = User::where('sso_id', '=', null)->get();
        $register = User::where('sso_id', '!=', null)->get();
           
        $redeem = Code::where('status', 'used')->get();
        $burn = Code::where('status', 'burned')->get();
           
//        $total = Post::sum('views');
           
        return view('dashboard')->withItem($item)->withLogin($login)->withRegister($register)->withRedeem($redeem)->withBurn($burn);
           
    }
    
    public function na(){
          for($i = 0; $i <= 3000; $i++){
            $ans = self::generateBarcodeNumber();
            $code = new Code;
            $code->code = $ans;
            $code->status = 'ready';
            $code->save();
    
    
  }
    }
    
    function barcodeNumberExists($number) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return Code::where('code', $number)->exists();
    }
    
     function generateBarcodeNumber() {
        $number = mt_rand(100000, 999999); 
    
        // call the same function if the barcode exists already
        if (self::barcodeNumberExists($number)) {
            return self::generateBarcodeNumber();
        }
    
        // otherwise, it's valid and can be used
        return $number;
    }

}
