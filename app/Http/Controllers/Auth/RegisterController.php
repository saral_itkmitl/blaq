<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\User;
use App\Wallet;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
//    protected function validator(array $data)
//    {
//        
//        return Validator::make($data, [
//            'name' => ['required', 'string', 'max:255'],
//            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
//            'phone' => ['required', 'string', 'email', 'min:10', 'max:10'],
//            'password' => ['required', 'string'],
//        ]);
//        
//        
//    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
//        dd ($request);
        $wallet = Wallet::where('phone', '=', $request->phone)->first();
            if($wallet != ''){
                $profile = new User;
                $profile->firstName = $request->first_name;
                $profile->surName   = $request->last_name;
                $profile->wallet_id = $wallet->id;
                $profile->phone = $request->phone;
                $profile->email = $request->email;
                $profile->password = bcrypt($request->password);
                $profile->save();
                return true;
            }else{
                    $wallet_new = new Wallet;
                    $wallet_new->phone  = $request->phone;
                    $wallet_new->balance = '200';
                    $wallet_new->save();
                
                    $wallet_id = Wallet::where('phone', '=',$request->phone)->first()->id;
                    $profile = new User;
                    $profile->firstName = $request->first_name;
                    $profile->surName   = $request->last_name;
                    $profile->wallet_id = $wallet_id;
                    $profile->phone = $request->phone;
                    $profile->email = $request->email;
                    $profile->password = bcrypt($request->password);
                    $profile->save();
                return true;
                
    
            }

    }
    
    public function register(Request $request)
    {
        
//        $this->validator($request->all())->validate();
        
        if(self::confirmOTP($request)){
            
            $user = self::create($request);
            
           
            
            Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->_token);
            Session::flash('message', 'Register successful');
            return Redirect::to('/');
        }else{
            Session::flash('message', 'OTP not match, Please try again.');
                return View::make('otp')->with('profile', $request);
        }

        
    }
    
    public function registerSSO(Request $request)
    {	
        try{
             $splitName = explode(' ', $request->name, 2);
             $first_name = $splitName[0];
             if(empty($splitName[1])){
                 $last_name = $first_name;
             }else{
                 $last_name = $splitName[1];
             }
             
             
            /***** register to API *****/
            $registerInfo = new Client();
//            $res_backoffice = $registerInfo->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/register',    //staging
             $res_backoffice = $registerInfo->post('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/register',    //production
                [
                    'form_params' =>  [
                        'registration_type' => 'rewards_blaq',
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'phone' => $request->phone,
                        'language' => 'th',
                        'email' => $request->email,
                        'password' => $request->password,
                        'is_tc_accepted' => true,
                    ],
                ]);
               
        
            $profile = new User;
            $profile->firstName = $first_name;
            $profile->surName   = $last_name;
            $profile->phone = $request->phone;
            $profile->email = $request->email;
            $profile->password = $request->password;
            
            
            
            return  View::make('otp')->with('profile', $profile);
        }catch (\Exception $e){
            Session::flash('message', 'Please try another email and fill real phone number');
            return View::make('welcome');
            }
        
            
	}




    public function confirmOTP(Request $request){

        /*********** Check Verification email and save to SSO ********/
        $verifyOTP = new Client();
//        $token = 'mGgtDgdEX4q48cKBk9X5DvPeS4mW8xsz';    //staging token
         $token = 'BuY4sftGPxJZ6ymCka7fvbYUN5aMZsFd';    //production token
        try {
              /***** stagging ******/
//             $res = $verifyOTP->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/activation/phone',
              /***** production *****/
               $res = $verifyOTP->post('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/activation/phone',
                             [
                                 'headers' => [
                                   'Authorization' => 'Bearer ' . $token ],
                                 'form_params' =>  [
                                   'phone' => $request->phone,
                                   'code' => $request->code,
                                 ],
                             ]);
                
                Session::flash('message', 'Login Successful');
                return true;

        }catch (\Exception $e){
                return false;
            }
  
    }
    
     public function GetCurrentProfile($email){
        
        try{
            $getProfile = new Client();
//        $token = 'qZqu0EK8aL09xTiirvSyC0cKr59jVCId';    //staging token
        $token = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z';    //production token
        /***** stagging ******/
//        $res = $getProfile->get('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards/users/email/'.$email,
        /***** production *****/
        $res = $getProfile->get('https://sso.rabbit.co.th/v1/sso-user/rewards/users/email/'.$email,
                       [
                           'headers' => [
                           'Authorization' => 'Bearer ' . $token ]
                       ]);

        $data = $res->getBody()->getContents();

        $json = json_decode($data,true);

        return '1';
        }catch (\Exception $e){
            return '0';
        }
        
    }
    
    public function resend(Request $request){
            $updatePhone = new Client();
            /***** Update 5 data except email to SSO *****/

//            $res_phone = $updatePhone->patch('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards/activation/resend/phone',    //staging
            $res_phone = $updatePhone->patch('https://sso.rabbit.co.th/v1/sso-user/rewards/activation/resend/phone',    //production
                                [
                                    'form_params' =>  [
                                        'phone' => $request->phone,
                                        'language' => 'th'
                                    ],
                                ]);

            $code = $res_phone->getStatusCode();
            return back();
    }
    
    
}
