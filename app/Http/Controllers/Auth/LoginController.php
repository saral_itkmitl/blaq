<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use App\User;
use App\Wallet;
use Session;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
      public function loginSSO($request)
    {
        
        try {
            $email = $request->email;   //Get email form login page
            $password = $request->password;   //Get password form login page
                /*** Do login  **/
            $client    = new Client();
            
//            $token = 'qZqu0EK8aL09xTiirvSyC0cKr59jVCId';   //staging
      			$token     = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z'; //production
//            $res       = $client->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-auth/rewards/login',    //staging
            $res       = $client->post('https://sso.rabbit.co.th/v1/sso-auth/rewards/login',    //production
                [
                    'headers' => [
                        //                                    'Content-Type' => 'application/json', //  it's got error 422
                        'Authorization' => 'Bearer ' . $token
                                 ],
                    'form_params' => [
                        'credential' => $email,
                        'password' => $password],
                ]);
            
            $body   = $res->getBody()->getContents();
            $data = json_decode($body,true);

            $current   = self::GetCurrentProfile($request->email);
            
        
                
            if(User::where('email', '=', $request->email)->doesntExist()){
                
                /**get current profile ด้วยนะ***/
                
              $profile                  = new User(); 
              $profile->firstName       = $current['first_name'];
              $profile->surName         = $current['last_name'];
              $profile->email           = $current['email'];
              $profile->phone           = $current['phone'];
              $profile->sso_id          = $data['data']['sid'];
              $profile->password        = bcrypt($password);
                
            $wallet = Wallet::where('phone', '=', $current['phone'])->first();
            if($wallet != ''){
                
                $profile->wallet_id = $wallet->id;
            }else{
                    $wallet_new = new Wallet;
                    $wallet_new->phone  = $current['phone'];
                    $wallet_new->balance = '200';
                    $wallet_new->save();
                    
                    $wallet_id = Wallet::where('phone', '=', $current['phone'])->first();
                    $profile->wallet_id = $wallet_id->id;
            }
            
            $profile->save();
            
            }
            
            
            return  true;
            
//            return url()->previous();
        }catch (\GuzzleHttp\Exception\RequestException $e) {
                Session::flash('message', 'You entered wrong email or password');
            return  Redirect::to('/');
         }
    

    }
    
    
    public function __construct()
    {
        
        $this->middleware('guest')->except('logout');
    }
    
    
    public function GetCurrentProfile($email){
        $getProfile = new Client();
//        $token = 'qZqu0EK8aL09xTiirvSyC0cKr59jVCId';    //staging token
        $token = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z';    //production token
        /***** stagging ******/
//        $res = $getProfile->get('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards/users/email/'.$email,
        /***** production *****/
        $res = $getProfile->get('https://sso.rabbit.co.th/v1/sso-user/rewards/users/email/'.$email,
                       [
                           'headers' => [
                           'Authorization' => 'Bearer ' . $token ]
                       ]);

        $data = $res->getBody()->getContents();

        $json = json_decode($data,true);

        return $json['data'];
    }

}
