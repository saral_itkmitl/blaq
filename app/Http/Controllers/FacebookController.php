<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Wallet;
use App\User;
use App\Item;
use Socialite;
use Session;

class FacebookController extends Controller
{

        public function __construct()
    {
        $this->middleware('guest');
    }
    
    
   public function fbRedirect()
            
    {
            try {
                return Socialite::driver('facebook')->redirect();
            }catch (\Exception $e){
               
                Redirect::to('/');
            }
    }
    
    
        public function callback()
    {
        try{ 
            
            
            $user  = Socialite::driver('facebook')->user();
            $token = $user->token;
        
            $item = Item::all();
            $session_token = session()->getId();
            if(empty($user['email'])){
             
                 return Redirect::to('/cant-get-facebook-email');
            }
            if(self::CheckFacebookToken($token)){
               $login = User::where('email', '=', $user->user['email'])->first();
                if(!empty($login)){
					 
					Auth::loginUsingId($login->id);
                     return Redirect::to('/');
                }else{
                    
                      $current   = self::GetCurrentProfile($user->user['email']);

                      $profile                  = new User(); 
                      $profile->firstName       = $current['first_name'];
                      $profile->surName         = $current['last_name'];
                      $profile->email           = $current['email'];
                      if($current['phone'] != ''){
                          $profile->phone           = $current['phone'];
                      }else{
                           $profile->phone           = $user->user['id'];
                      }
                      $profile->password        = $user->user['id'];

                    $wallet = Wallet::where('phone', '=', $current['phone'])->first();
                    if($wallet != ''){

                        $profile->wallet_id = $wallet->id;
                        
                    }else{
                            $wallet_new = new Wallet;
                            $wallet_new->phone  = $current['phone'];
                            $wallet_new->balance = '200';
                            $wallet_new->save();

                            $wallet_id = Wallet::where('phone', '=', $current['phone'])->first();
                            $profile->wallet_id = $wallet_id->id;
                    }

                            $profile->save();

                            $logedin = User::where('email', '=', $user->user['email'])->first();
							Auth::loginUsingId($logedin->id);
							return Redirect::to('/');
                    }
                
                }else{
                        return View::make('register-fb')->withProfile($user->user);
                }
  
         }catch (\Exception $e){  
            Session::flash('message', 'Please register again with REAL PHONE NUMBER or Try with another email');
              return Redirect::to('/');
          }   
    }
    
    
        public function CheckFacebookToken($facebookToken){
           try {  
//               return $token;
//                 $token = 'qZqu0EK8aL09xTiirvSyC0cKr59jVCId';    //staging token
        $token = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z';    //production token
                  $fbToken = new Client();
//                  $res_fb = $fbToken->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-auth/rewards/login/fb',    //staging
        $res_fb = $fbToken->post('https://sso.rabbit.co.th/v1/sso-auth/rewards/login/fb',    //production
                            [   
                                'headers' => [
                                   'Authorization' => 'Bearer ' . $token ],
                                'form_params' =>  [
                                    'fb_token' => $facebookToken,
                                ],
                            ]);
              
//              $data   = $res_fb->getBody()->getContents();
//              $json = json_decode($data,true);
//               dd($data);
               
//              return $json['data']['sid'];
              return true;
           }catch (\Exception $e){  
              return false;
              
          } 
        }
    
  

    public function GetCurrentProfile($email){
        $getProfile = new Client();
//        $token = 'qZqu0EK8aL09xTiirvSyC0cKr59jVCId';    //staging token
        $token = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z';    //production token
        /***** stagging ******/
//        $res = $getProfile->get('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards/users/email/'.$email,
        /***** production *****/
        $res = $getProfile->get('https://sso.rabbit.co.th/v1/sso-user/rewards/users/email/'.$email,
                       [
                           'headers' => [
                           'Authorization' => 'Bearer ' . $token ]
                       ]);

        $data = $res->getBody()->getContents();

        $json = json_decode($data,true);

        return $json['data'];
    }

    
    
}
