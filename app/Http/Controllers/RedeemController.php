<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Item;
use App\Code;
use Auth;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
class RedeemController extends Controller
{

       public function __construct()
    {
        $this->middleware('auth');
    }

     public function redeemList()
     {
         $item = Item::all();
         return view('redeem')->withItem($item);
     }
    
    
    public function redeem($id)
    {

        $wallet = Auth::user()->wallet;
        $balance = $wallet->balance;
        $item = Item::where('id', '=', $id)->first();
        $price = $item->price;
        if($balance >= $price){
            if($item->stock > '0'){
                $code = Code::where('status', '=', 'ready')->first();
                if(!empty($code)){
                    $code->update(["status" => 'used']); 
                    $item->update(["stock" =>  $item->stock - 1]);
                    
                    $new_balance  = $balance - $price;

                    $transaction                  = new Transaction(); 
                    $transaction->user_id         = Auth::id();
                    $transaction->wallet_id       = $wallet->id;
                    $transaction->item_id         = $id;
                    $transaction->code_id         = $code->id;
                    $transaction->save();

                    $wallet->update(["balance" => $new_balance]);
                    Session::flash('message', 'Redeem Successful !');

                    $item_show = Transaction::where('user_id', '=', Auth::id())->get();

                    return  Redirect::to('/my-redeem')->withItem($item_show);

                }else{
                    Session::flash('message', 'Out of stock');
                    return  Redirect::to('/redeem-list');
                }

            }else{
                 Session::flash('message', 'Out of stock');
                 return  Redirect::to('/redeem-list');
            }


        }else{
            Session::flash('message', 'Not enough points');
       
            return  Redirect::to('/redeem-list');
        }
        
        
            
            
    }
    
    public function burnCode($transaction_id)
    {
        $user_id = Auth::id();
        
        $item = Transaction::find($transaction_id);
        
        $item->code->update(["status" => 'burned']);
       
        
        
        Session::flash('message', 'Burned code successful');
        
        $mytime = date('Y-m-d H:i:s');
        
        $item->update(["updated_at" => $mytime]); 
        
        return  Redirect::to('/my-redeem');

    }
    
    
     public function cancelRedeem($transaction_id)
    {
        $item = Transaction::find($transaction_id);
         
        $stock = $item->item->stock;
        $refund = $item->item->price;
        $wallet = $item->wallet->balance;
         
        $item->code->update(["status" => 'ready']);
       
        $item->item->update(["stock" => $stock + 1]);
        
        $item->wallet->update(["balance" => $wallet + $refund]);
         
        $item->delete();
         
        Session::flash('message', 'Cancel successful');
         
        return Redirect::to('/my-redeem');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function myRedeem()
    {
        $id = Auth::id();
        $item = Transaction::where('user_id', '=', $id)->orderBy('updated_at', 'DESC')->get();
//        if(count($item) == '0'){
//            Session::flash('message', 'There is not have any redeem item yet');
//            return View::make('my-redeem')->withItem($item);
//        }
        return View::make('my-redeem')->withItem($item);
    }
}
