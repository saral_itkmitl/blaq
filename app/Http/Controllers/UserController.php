<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
//use Session;
use App\Member;
use App\Wallet;
class UserController extends Controller
{
    public function login(Request $request)

    {
       
        try {
            $email = $request->email;   //Get email form login page
            $password = $request->password;   //Get password form login page
                /*** Do login  **/
            $client    = new Client();
            

             $token = 'qZqu0EK8aL09xTiirvSyC0cKr59jVCId';   //staging
      //			$token     = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z'; //production
            $res       = $client->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-auth/rewards/login',    //staging
//            $res       = $client->post('https://sso.rabbit.co.th/v1/sso-auth/rewards/login',    //production
                [
                    'headers' => [
                        //                                    'Content-Type' => 'application/json', //  it's got error 422
                        'Authorization' => 'Bearer ' . $token
                                 ],
                    'form_params' => [
                        'credential' => $email,
                        'password' => $password],
                ]);
            $data   = $res->getBody()->getContents();
            
            
            
            $resProfile     = $client->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards/users/email/'.$email,    //staging
//            $resProfile       = $client->post('https://sso.rabbit.co.th/v1/sso-user/rewards/users/email/'.$email,
                [
                    'headers' => [
                        //                                    'Content-Type' => 'application/json', //  it's got error 422
                        'Authorization' => 'Bearer ' . $token
                                 ]
                ]);
            $current   = $resProfile->getBody()->getContents();
            
            $member   = Member::where('email', '=', $request->email)->first();
                
            if($member != ''){
                
                /**get current profile ด้วยนะ***/
                
              $profile                  = new Member(); 
              $profile->firstName       = $current->firstName;
              $profile->surName         = $current->surName;
              $profile->email           = $current->email;
              $profile->phone           = $current->phone;
              $profile->sso_id          = $data['data']['sid'];
                
            $wallet = Wallet::where('tel', '=', $request->phone)->first();
            if($wallet != ''){
                
                $profile->wallet_id = $wallet->id;
            }else{
                    $wallet_new = new Wallet;
                    $wallet_new->phone  = $request->phone;
                    $wallet_new->balance = '200';
                    $wallet_new->save();
                
                    $wallet_id = Wallet::where('tel', '=', $request->phone)->first()->id;
                    $profile->wallet_id = $wallet_id;
            }
            
            $profile->save();
            
            }
            
            Session::flash('message', 'เข้าสู่ระบบสำเร็จ');
            return  View::make('redeem')->with('profile', profile);
//            return url()->previous();
        }catch (\GuzzleHttp\Exception\RequestException $e) {
    //            Session::flash('message', 'ไม่สามารถเข้าสู่ระบบ กรุณาตรวจสอบอีเมลล์และรหัสผ่านของคุณ');
            return  Redirect::to('/')->with('message', 'ไม่สามารถเข้าสู่ระบบ กรุณาตรวจสอบอีเมลล์และรหัสผ่านของคุณ');
         }

    }


    public function register(Request $request)
    {	
        try{
        
            /*** Current Profile ****/
              $profile                  = new Member(); 
              $profile->name            = $request->name;
              $profile->surname         = $request->surname;
              $profile->email           = $request->email;
              $profile->phone           = $request->phone;
              
            /***** register to API *****/
            $registerInfo = new Client();
            $res_backoffice = $registerInfo->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/register',    //staging
            // $res_backoffice = $registerInfo->post('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/register',    //production
                [
                    'form_params' =>  [
                        'registration_type' => 'rewards_blaq',
                        'first_name' => $request->first_name,
                        'last_name' => $request->last_name,
                        'phone' => $request->phone,
                        'language' => 'th',
                        'email' => $request->email,
                        'password' => $request->password,
                        'is_tc_accepted' => true,
                    ],
                ]);
            
                
            $wallet = Wallet::where('tel', '=', $request->phone)->first();
            if($wallet != ''){
                
                $profile->wallet_id = $wallet->id;
            }else{
                    $wallet_new = new Wallet;
                    $wallet_new->phone  = $request->phone;
                    $wallet_new->balance = '200';
                    $wallet_new->save();
                
                    $wallet_id = Wallet::where('tel', '=', $request->phone)->first()->id;
                    $profile->wallet_id = $wallet_id;
            }
            
            $profile->save();
            
            return  View::make('otp')->withPhone($request->phone);
        }catch (\Exception $e){
            return Redirect::to('/')->with('message', 'เกิดข้อผิดพลาดระหว่างบันทึกข้อมูล กรุณาลองใหม่อีกครั้ง');
            }
        
	}




    public function VerifyOTP(Request $request){

        /*********** Check Verification email and save to SSO ********/
        $verifyOTP = new Client();
        $token = 'mGgtDgdEX4q48cKBk9X5DvPeS4mW8xsz';    //staging token
        // $token = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z';    //production token
        try {
              /***** stagging ******/
             $res = $verifyOTP->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/activation/phone',
              /***** production *****/
            //   $res = $verifyOTP->post('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/activation/phone',
                             [
                                 'headers' => [
                                   'Authorization' => 'Bearer ' . $token ],
                                 'form_params' =>  [
                                   'phone' => $request->phone,
                                   'code' => $request->code,
                                 ],
                             ]);
                Session::flash('message', 'เข้าสู่ระบบสำเร็จ');
                return view('thanks');

        }catch (\Exception $e){
                Session::flash('message', 'รหัสไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง');
                return view('VerifyOTP')->withPhone($request->phone);
            }
  
    }
    public function CheckEmail(Request $request)
    {
        $email  = $request->email;
        $client = new Client();
//        $res    = $client->get('https://sso.rabbit.co.th/v1/sso-user/kiosk/user/email/status/'.$email); //production
        $res    = $client->get('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/kiosk/user/email/status/'.$email); //staging
        $data   = $res->getBody()->getContents();
        $json   = json_decode($data,true);
        return $json['data']['status']; //return 0 or 1
    }
	
	public function ResendOTP(Request $request){
        $updatePhone = new Client();
        /***** Update 5 data except email to SSO *****/

        $res_phone = $updatePhone->patch('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards/activation/resend/phone',    //staging
//        $res_phone = $updatePhone->patch('https://sso.rabbit.co.th/v1/sso-user/rewards/activation/resend/phone,    //production
                            [
                                'form_params' =>  [
                                    'phone' => $request->phone,
                                    'language' => 'th'
                                ],
                            ]);

        $code = $res_phone->getStatusCode();
        return $code;
	}
    
        public function fbRedirect()
            
    {
            
            try {
                return Socialite::driver('facebook')->redirect();
            }catch (\Exception $e){
                Redirect::to('/')->with('message', 'เกิดข้อผิดพลาดระหว่างบันทึกข้อมูล กรุณาลองใหม่อีกครั้ง');
            }
            
    }
    
        public function callback()
    {
        try{ 
            
            $user  = Socialite::driver('facebook')->user();
            $token = $user->token;

//            dd($user);
            $response = self::CheckFacebookToken($token);
            return $response;

           return $token;
         }catch (\Exception $e){  
              Redirect::to('/')->with('message', 'เกิดข้อผิดพลาดระหว่างบันทึกข้อมูล กรุณาลองใหม่อีกครั้ง');
          }   
    }
        public function CheckFacebookToken($facebookToken){
           try {  
//               return $token;
                 $token = 'qZqu0EK8aL09xTiirvSyC0cKr59jVCId';    //staging token
//        $token = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z';    //production token
                  $fbToken = new Client();
                  $res_fb = $fbToken->post('https://staging-sso-sso.rabbitinternet.com/v1/sso-auth/rewards/login/fb',    //staging
//        $res_fb = $fbToken->post('https://sso.rabbit.co.th/v1/sso-auth/rewards/login/fb',    //production
                            [   
                                'headers' => [
                                   'Authorization' => 'Bearer ' . $token ],
                                'form_params' =>  [
                                    'fb_token' => $facebookToken,
                                ],
                            ]);
              
              $data   = $res_fb->getBody()->getContents();
              return $data;
           }catch (\Exception $e){  
              return 'error';
          } 
        }

    
}


