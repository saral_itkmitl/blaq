<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

     protected $fillable = [
        'updated_at',
    ];
        Public function item()
    {
        return $this->hasOne('App\Item', 'id', 'item_id');
    }

         Public function code()
    {
        return $this->hasOne('App\Code', 'id', 'code_id');
    }

        Public function wallet()
    {
        return $this->hasOne('App\Wallet', 'id', 'wallet_id');
    }

}
