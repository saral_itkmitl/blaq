<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use GuzzleHttp\Client;
use App\Member;
use App\Transaction;
use App\Item;
Route::get('/test', function () {
    

});
      
Route::get('/home', function () {
        return redirect('/');
});

//Route::get('/fb', function () {
//        return view('register-fb');
//});


Route::get('/', function () {
    if(Auth::check()){
        return redirect('/redeem-list');
    }else{
        return redirect('/login');
        }
});

Route::get('/callback-fb', 'FacebookController@callback');

Route::get('/redirect-fb', 'FacebookController@fbRedirect');

Route::get('/verify-otp', function () {
    return view('otp');
});

Route::get('/contact', function () {
    return view('contact');
});
Route::get('/cant-get-facebook-email', function () {
    return view('cantfb');
});



//Route::post('/otp', function () {
//    return view('otp');
//});

Route::get('/log_out', function () {
    Auth::Logout();
        Session::flush();
        return redirect('/');
});



Route::get('/gengen','DashboardController@na');

Route::get('/dashboard','DashboardController@dashboard');

Route::get('/redeem-list','RedeemController@redeemList');
//Route::get('/my-redeem', function () {
//    
//    return view('redeem');
//});

Route::get('/my-redeem','RedeemController@myRedeem');

//Route::get('/my-redeem', function () {
//    return view('my-redeem');
//});

Route::get('/burn_code/{id}','RedeemController@burnCode');
Route::post('/burn_code/{id}','RedeemController@burnCode');

//Route::get('/verify-otp','Auth\RegisterController@registerSSO');
Route::post('/verify-otp','Auth\RegisterController@registerSSO');

//Route::get('/confirm-otp','Auth\RegisterController@confirmOTP');

Route::post('/redeem/{id}','RedeemController@redeem');

Route::get('/redeem/{id}','RedeemController@redeem');

//Route::get('/login','UserController@login');
//
//Route::post('/login','UserController@login');

Route::get('/cancel_redeem/{id}','RedeemController@cancelRedeem');
Route::post('/cancel_redeem/{id}','RedeemController@cancelRedeem');

//Auth::routes();
Route::get('login', [
  'as' => 'login',
  'uses' => 'Auth\LoginController@showLoginForm'
]);

Route::post('login', [
  'as' => '',
  'uses' => 'Auth\LoginController@login'
]);

Route::post('register', [
  'as' => '',
  'uses' => 'Auth\RegisterController@register'
]);

Route::get('register', [
  'as' => 'register',
  'uses' => 'Auth\RegisterController@showRegistrationForm'
]);

//Route::get('register', [
//  'as' => 'register',
//  'uses' => 'Auth\RegisterController@register'
//]);
//
//Route::post('/registering','Auth\RegisterController@register');
//Route::get('/registering','Auth\RegisterController@register');
    
Route::post('/OTP-Verify','UserController@VerifyOTP');

Route::get('/CheckEmail','UserController@CheckEmail');

Route::get('/search','DashboardController@search');

Route::post('/CheckEmail','UserController@CheckEmail');

Route::get('/resend','Auth\RegisterController@resend');
Route::post('/resend','Auth\RegisterController@resend');
Route::patch('/resend','Auth\RegisterController@resend');