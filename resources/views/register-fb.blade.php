@extends('layouts.app')

@section('content')

<div class="container" style="padding-top:10px ;text-align:center;">

 

<div class="login-wrap" style="text-align:center;">
<img src="img/blaq-lyte-logo.png"  style="width:90%;">
	<div class="login-html" style="text-align:center;">
        @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-warning') }}">{{ Session::get('message') }}</p>

        @endif
		
		<input id="tab-2" type="radio" name="tab" class="sign-up" checked><label for="tab-2" class="tab"></label>

		<div class="login-form" >

			<div class="sign-up-htm">

                <form action="{{ url('/verify-otp')}}" id="register" method="post" >
                         @csrf
                    <div class="group">
                        <input name="name" id="user" value="{{$profile['name']}}" type="text" class="input" required>
                    </div>
                 
                    
                 
                    
                    <div class="group">
                        <input name="email" id="checkEmail" id="email" value="{{$profile['email']}}"  maxlength="50" type="email" class="input" required>
                        <div class="check_result">
                             <span id="user-result"></span>
                        </div>
                       
                    </div>
                    
                       
                    <div class="group">
    <!--					<label for="pass" class="label">Email Address</label>-->
                        <input name="phone" id="checkPhone" id="phone" placeholder="phone number" onkeypress='return event.charCode >= 48 && event.charCode <= 57' minlength="10" maxlength="10" type="tel" class="input" required autofocus>
                    
                           <div class="check_result">
                             <span id="user-result2"></span>
                             </div>
                    </div>
                    
                    <div class="group">
    <!--					<label for="pass" class="label">Password</label>-->
                        <input name="password" autocomplete="off" minlength="6" maxlength="12" id="pass" placeholder="password" type="password" class="input" data-type="password " required>
                    </div>


                    
                    
                        <button id="button-sub" onclick="checkSubmit()"  id="register" type="submit" class="btn btn-primary" style="background-color: #fd8204;
    border-color: #d2893e;font-size:18px;border-radius:25px;height:50px;width:100%;margin-bottom:12px;letter-spacing: 1.0px;" >Register</button>

                </form>

			</div>
            
		</div>
	</div>
     
</div>
     
<br>
<br><br><br>
    
       <img src="img/ripndip-logo.png"  style="width:80%;margin:auto;max-width:600px;">
    </div>

@endsection