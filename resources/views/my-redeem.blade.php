@extends('layouts.app')

@section('content')
<div class="container">
    
 
    

<div class="login-wrap" style="min-height: 420px; padding:10px ;text-align:center;margin-bottom:auto;padding-top:0px;">
    
    @include('layouts.menu')
    
    <div style="text-indent: 0px;line-height: 1.0;padding:1px;text-align:center;font-size:25px;color:#fff;font-weight: 450;">
        
        @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-success') }}" style="font-weight: 400;font-size:18px;">{{ Session::get('message') }}</p>

        @endif
        
        <h3 >My Rewards</h3>
     @if(count($item) == '0')
        
            <h6 class="modal-title"  style="margin:70px auto 0px auto;color:white;">&nbsp;There is not have any rewards yet</h6>
        
         <br>
                <a href="{{url('/')}}"  class="btn btn-sm btn-primary" style="background-color: #fd8204;border-color: #fd8204;border-radius:25px;padding:5px 8px 5px 8px;margin-bottom:20px;" >Redeem now</a>
       
<br> <br>
     @else
        <a style="text-indent: 0px;line-height: 1.0;margin-bottom:50px;text-align:center;font-size:16px;color:#dc3545;margin:auto;font-weight: 500;">Please show this page at counter.</a></div><br>
    <div style="margin-bottom:10px;">
     @foreach ($item as $i)
             <figure class="card card-product">
<div class="row">
<div class="col-6" style="text-align:center;margin:auto;padding-right:0px;">

<div class="pic" >
		<img class="item-pic" src="{{$i->item->item_path}}" />
     
    </div>
    
</div>
     @if($i->code->status == 'used')
        <div class="col-6" style="text-align:center;margin:auto;padding-right:25px;padding-left:0px;margin-bottom:10px;margin-top:15px;">
            <a style="font-size:19px;font-weight: 450;">{{$i->item->itemName}}<br></a>
            
           
           
                   <div class="login-form" style="item-align:center;min-height:0px;">
                       <div class="group" style="margin-bottom:12px;">

					  <span style="font-size:13px;color:#000;">Exp. 21-01-19 02:00</span>

				       </div>
                   </div>
                              
           
                    <!--burn Modal -->
                    <div class="modal fade" id="myModal{{$i->id}}" role="dialog" >
                        <div class="modal-dialog">
                        
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header" >
                         
                            <h4 class="modal-title"  style="margin:auto;">&nbsp;&nbsp;  For Staff Only !!</h4>
                                   <button type="button" style="margin:0;" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body" style="text-align:center;">
                                <div style="margin:8px;">
                                        <a style="font-size:21px;font-weight: 450;color:grey;">{{$i->code->code}}<br></a>
                                </div>
                                <br>
                                <p>DO NOT click on "Confirm burn code" button if you are not staff !!!</p>

                            </div>
                            <div class="modal-footer" style="margin:auto;">
                                
                                 <form action="{{url('/burn_code')}}/{{$i->id}}" id="form{{$i->id}}" style="margin-right:0px;">
                            @csrf
                          <button type="submit" id="form{{$i->id}}" value="Submit" data-toggle="modal"  class="btn btn-default btn-primary" style="background-color: #fd8204;
    border-color: #fd8204;border-radius:25px;padding:5px 8px 5px 8px;">Confirm burn code</button>
                         </form>
                                
                           
                            </div>
                            
                              <div class="" style="margin:auto;margin-bottom:15px;">
                                 <button  type="button"  class="btn btn-default" style="background-color: grey;
    border-color: grey;border-radius:25px;padding:5px 8px 5px 8px;margin:0;color:#fff;" data-dismiss="modal">cancel</button>
                            </div>
                            
                        </div>
                        
                        </div>
                    </div>
            <!-- End burn Modal -->
            
                <!--cancel Modal -->
                    <div class="modal fade" id="myModalCancel{{$i->id}}" role="dialog" >
                        <div class="modal-dialog">
                        
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header" >
                         
                            <h4 class="modal-title"  style="margin:auto;">&nbsp; Cancel redeem </h4>
                                   <button type="button" style="margin:0;" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body" style="text-align:center;">
           
                                <p>Are you sure to cancel redeem ?</p>

                            </div>
                            <div class="modal-footer" style="margin:auto;">
                                
                                 <form action="{{url('/cancel_redeem')}}/{{$i->id}}" id="formCancel{{$i->id}}" style="margin-right:0px;">
                            @csrf
                          <button type="submit" id="formCancel{{$i->id}}" value="Submit" data-toggle="modal"  class="btn btn-default btn-primary" style="background-color: #fd8204;
    border-color: #fd8204;border-radius:25px;padding:5px 8px 5px 8px;">Confirm</button>
                         </form>
                                
                                <br/>
                          
                            </div>
                                     <div class="" style="margin:auto;margin-bottom:15px;">
                                 <button  type="button"  class="btn btn-default" style="background-color: grey;
    border-color: grey;border-radius:25px;padding:5px 8px 5px 8px;margin:0;color:#fff;" data-dismiss="modal">cancel</button>
                            </div>
                        </div>
                        
                        </div>
                    </div>
             <!-- End cancel Modal -->
        
            
            
        <div style="margin-bottom:0px;">
                <button data-toggle="modal" data-target="#myModal{{$i->id}}"  class="btn btn-sm btn-primary" style="background-color: #fd8204;border-color: #fd8204;border-radius:25px;padding:5px 8px 5px 8px;" value="Submit">Burn code</button>
        </div>
           
            <div style="font-size:16px;text-decoration: underline;">
                
                <button style="text-decoration: underline;" class="btn btn-sm btn-link" data-toggle="modal" data-target="#myModalCancel{{$i->id}}" value="Submit">Cancel</button>
                
                
            </div>
            
            
            @else
            <div class="col-6" style="text-align:center;margin:auto;padding-right:25px;padding-left:0px;margin-bottom:10px;margin-top:26px;">
            <a style="font-size:19px;font-weight: 450;">{{$i->item->itemName}}<br></a>
            
           
           
                   <div class="login-form" style="item-align:center;min-height:0px;">
                       <div class="group">
                          
                        
                        <span style="font-size:13px;color:#000;">Burn : {{$i->updated_at->diffForHumans()}}</span>
                        <br>
<!--					  <span style="font-size:13px;color:#000;">Exp. 21-01-19 02:00</span><br>-->
                           <span style="font-size:13px;color:#000;">code : {{$i->code->code}}</span>

				       </div>
                   </div>
         <button   class="btn btn-sm btn-primary" style="background-color: grey;
    border-color: grey;border-radius:25px;padding:5px 8px 5px 8px;" value="Submit">Burned</button>
          <div style="margin-bottom:17px;"></div>
            
            
            @endif
	
          
<!--            <a style="font-size:13px;font-weight: 450;margin-right:10px;">* for staff</a>-->

</div> <!-- col // -->
</div>
            </figure>
            @endforeach
        @endif
                 <br> <br> 
                     <div style="text-align:left; width:100%;color:white; font-size:small;">
         <b>Terms &amp; Conditions.</b>
        <ol style="padding-left: 20px;    padding-inline-start: 20px;text-align:left; ">
            <li>Each user is entitled to 200 points only.</li>
            <li>These points can be used in Bangkok Block Party on January 19-20, 2019 only. The remaining points are non-transferable to Rabbit Rewards points.
            </li>
           
        </ol>
    </div>
	

</div>
   

</div>
  <div style="text-align:center;margin-top:20px;">
          <img src="img/ripndip-logo.png"  style="width:80%;margin:auto;max-width:600px;">
    </div>
@endsection