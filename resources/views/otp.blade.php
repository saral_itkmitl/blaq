@extends('layouts.app')

@section('content')
<div class="container">
    
<div class="login-wrap" style="min-height: 310px; padding:10px ;text-align:center;margin-bottom:105px;">
    <img src="img/blaq-lyte-logo.png"  style="width:90%;">
	<div class="login-html" style="text-align:center;padding:30px;">
        @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-warning') }}">{{ Session::get('message') }}</p>

                @endif
		<a style="text-indent: 0px;line-height: 1.8;padding:10px;margin-bottom:70px;text-align:center;font-size:18px;color:#fff;width:100%;">Please type the verfication code which we sent to {{$profile->phone}} </a>
		
        
		<div class="login-form" style="item-align:center;">
                
				<div class="sign-up-htm2">
                <form action="{{ route('register')}}"  method="POST">
<!--                <input name="_method" type="hidden" value="POST">-->
                 @csrf
                
                    <input type="hidden" name="first_name" value="{{$profile->firstName}}">
                    <input type="hidden" name="last_name" value="{{$profile->surName}}">
                    <input type="hidden" name="email" value="{{$profile->email}}">
                    <input type="hidden" name="phone" value="{{$profile->phone}}">
                    <input type="hidden" name="password" value="{{$profile->password}}">
				<div class="group">
<!--					<label for="user" class="label">Username</label>-->
					<input name="code" type="tel" maxlength="4" style="margin-top:20px;width:45%;margin-left:auto;margin-right:auto;" id="user" autofill="off" placeholder="X X X X" type="text" class="input" required>
				</div>
				
               
         
                
                     <button type="submit" onclick="click_button()" id="button-d" class="btn btn-primary" style="background-color: #fd8204; border-color: #d2893e;font-size:18px;border-radius:25px;height:50px;width:45%;margin-bottom:12px;letter-spacing: 1.0px;" >Confirm</button>
<!--				<div class="hr"></div>-->
				<div class="foot-lnk">
					<a id="button-d2"  href="javascript:location.reload(true)" id="" >resend</a>
				</div>
                    </form>
			</div>
            

		</div>
             
	</div>
</div>
      <img src="img/ripndip-logo.png"  style="width:80%;margin:auto;max-width:600px;">
    </div>

@endsection