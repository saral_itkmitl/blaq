@extends('layouts.app')

@section('content')
<div class="container">
<div class="login-wrap" style="min-height: 310px; padding:10px ;text-align:center;margin-bottom:105px;">
    
	<div class="login-html" style="text-align:center;padding:30px;">
        @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-warning') }}">{{ Session::get('message') }}</p>

                @endif
	 
		<div class="login-form" style="item-align:center;">
            
				<div class="sign-up-htm2">
                <div class="modal fade" id="myModal" role="dialog" >
                        <div class="modal-dialog">
                        
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header" >
                         
                            <h4 class="modal-title"  style="margin:auto;">&nbsp; For Staff Only !!</h4>
                                   <button type="button" style="margin:0;" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body" style="text-align:center;">
                                <div style="margin:8px;">
                                        <a style="font-size:21px;font-weight: 450;color:grey;">{{$tran->code->code}}<br></a>
                                </div>
                                <br>
                                <p>Staff Only</p>

                            </div>
                            <div class="modal-footer" style="margin:auto;">
                                
                        <form action="{{url('/refund')}}/{{$tran->id}}" id="form" style="margin-right:0px;">
                            @csrf
                          <button type="submit" id="form" value="Submit" data-toggle="modal"  class="btn btn-default btn-primary" style="background-color: #fd8204;
    border-color: #fd8204;border-radius:25px;padding:5px 8px 5px 8px;">Confirm refund</button>
                         </form>
                                
                           
                            </div>
                            
                              <div class="" style="margin:auto;margin-bottom:15px;">
                                 <button  type="button"  class="btn btn-default" style="background-color: grey;
    border-color: grey;border-radius:25px;padding:5px 8px 5px 8px;margin:0;color:#fff;" data-dismiss="modal">cancel</button>
                            </div>
                            
                        </div>
                        
                        </div>
                    </div>
            <!-- End burn Modal -->
            
     
        <div style="margin-bottom:0px;">
                <button data-toggle="modal" data-target="#myModal"  class="btn btn-sm btn-primary" style="background-color: #fd8204;border-color: #fd8204;border-radius:25px;padding:5px 8px 5px 8px;" value="Submit">refund code</button>
        </div>
           
			</div>
            

		</div>
             
	</div>
</div>
    </div>

@endsection