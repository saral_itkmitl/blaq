@extends('layouts.app')

@section('content')

<div class="container" style="padding-top:10px ;text-align:center;">

 

<div class="login-wrap" style="text-align:center;">
<img src="img/blaq-lyte-logo.png"  style="width:90%;">
	<div class="login-html" style="text-align:center;" id="min-h">
        @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-warning') }}">{{ Session::get('message') }}</p>

        @endif
        
        
        
     
           
		<input id="tab-1" type="radio" name="tab" class="sign-in"  checked><label for="tab-1" class="tab"></label>
		<input id="tab-2" type="radio" name="tab" class="sign-up"  ><label for="tab-2" class="tab"></label>


            

        		<div class="login-form" >
                    
     
			<div class="sign-in-htm" >
                  <div class="group" style="margin-bottom:0px;">
                      	<form action="{{url('/redirect-fb')}}">
                      <button onclick="click_button()" id="button-d" class="btn btn-primary loginBtn--facebook" style="font-size:16px;border-radius:25px;height:50px;width:100%;margin-bottom:12px;letter-spacing: 1.0px;padding-top:12px;" ><span style="margin:3px 6px 5px 4px;padding-bottom:3px;border-right: 1px solid rgba(215, 202, 202, 0.37);"><img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_facebook.png" style="vertical-align: sub;" max-height="14" max-width="14"> </span>&nbsp;Login with Facebook</button>
                       </form>
                </div>
                    <div class="login-or">
                        <hr class="hr-or">
                        <span class="span-or">OR</span>
                     </div>

                <form action="{{ route('login')}}" id="login" method="POST">
                 @csrf
				<div class="group">

					<input name="email" value="{{ old('email') }}"  autofill="off" maxlength="50"  placeholder="email or rabbit card" type="text" class="input" required>
				</div>
				<div class="group">

					<input name="password" id="pass" autocomplete="off" minlength="4" maxlength="16"  placeholder="password" type="password" class="input" data-type="password" required>
				
                </div>


					<button onclick="click_button()" id="button-d2" type="submit" id="login" class="btn btn-primary" style="background-color: #fd8204;border-color: #d2893e;font-size:18px;border-radius:25px;height:50px;width:100%;margin-bottom:12px;letter-spacing: 1.0px;" >Login</button>

                </form>

<!--				<div class="hr"></div>-->
				<div class="foot-lnk">
					<a href="https://id.rabbit.co.th/en/password/reset" >forget password?</a> or 
                 <label onclick="fb_login()" for="tab-2" ><a style="text-decoration: underline;">Register</a></label>
				</div>
                 <div style="text-align:left; width:100%;color:white; font-size:small;">
                     <hr>
        <b>Terms &amp; Conditions.</b>
        <ol style="padding-left: 20px;    padding-inline-start: 20px;text-align:left; ">
            <li>Each user is entitled to 200 points only.</li>
            <li>These points can be used in Bangkok Block Party on January 19-20, 2019 only. The remaining points are non-transferable to Rabbit Rewards points.
            </li>
           
        </ol>
    </div>
			</div>
            
					
				


			<div class="sign-up-htm">

 <div class="group" style="margin-bottom:0px;">      
                      <form action="{{url('/redirect-fb')}}">
                      <button onclick="click_button()" id="button-d3" class="btn btn-primary loginBtn--facebook" style="font-size:16px;border-radius:25px;height:50px;width:100%;margin-bottom:12px;letter-spacing: 1.0px;padding-top:12px;" ><span style="margin:3px 6px 5px 4px;padding-bottom:3px;border-right: 1px solid rgba(215, 202, 202, 0.37);"><img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/14082/icon_facebook.png"  style="vertical-align: sub;" max-height="14" max-width="14"> </span>&nbsp;Register with Facebook</button>
                       </form>
                    </div>
                    <div class="login-or">
                        <hr class="hr-or">
                        <span class="span-or">OR</span>
                     </div>


                <form action="{{ url('/verify-otp')}}" id="register" method="post" >
                         @csrf
                    
                    <div class="group">
                        <input name="name"  value="{{ old('name') }}" placeholder="full name" type="text" class="input" required>
                    </div>
                    
                    <div class="group">
    <!--					<label for="pass" class="label">Email Address</label>-->
                        <input name="phone" value="{{ old('phone') }}" id="checkPhone" id="phone" placeholder="phone number" onkeypress='return event.charCode >= 48 && event.charCode <= 57' minlength="10" maxlength="10" type="tel" class="input" required>
                    
                           <div class="check_result">
                             <span id="user-result2"></span>
                             </div>
                    </div>
                    
                 
                    
                    <div class="group">
                        <input value="{{ old('email') }}" name="email" id="checkEmail" id="email" placeholder="email"  maxlength="50" type="email" class="input" required>
                        <div class="check_result">
                             <span id="user-result"></span>
                        </div>
                       
                    </div>
                    
                    <div class="group">
    <!--					<label for="pass" class="label">Password</label>-->
                        <input name="password" autocomplete="off" minlength="6" maxlength="12" id="pass" placeholder="password" type="password" class="input" data-type="password " required>
                    </div>


                    
                    
                        <button id="button-sub" id="register" onclick="checkSubmit()" type="submit" class="btn btn-primary" style="background-color: #fd8204;
    border-color: #d2893e;font-size:18px;border-radius:25px;height:50px;width:100%;margin-bottom:12px;letter-spacing: 1.0px;" >Register</button>

                </form>
                
              

<!--				<div class="hr"></div>-->
				<div class="foot-lnk">
					<label for="tab-1" onclick="fb_register()">already member? <a style="text-decoration: underline;">Click Here</a></label>
				</div>
			</div>
            
		</div>
	</div>
     
   
</div>
     
<br>
<br><br><br>
    <br><br>
	    <div style="background:#fff;max-width: 170px;margin:105px auto 25px auto ;min-height:35px;">
      <a href="https://id.rabbit.co.th/en/register"><img src="img/rrlogo.png" style="max-width: 160px;margin:auto;margin-top:5px;"></a>
    </div>

    <img src="img/ripndip-logo.png"  style="width:80%;margin:auto;max-width:600px;margin-top:10px;">
    </div>

@endsection