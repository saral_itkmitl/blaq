<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
      <meta charset="utf-8">
        <meta name="viewport" content="width=100%, initial-scale=1">
        <meta property="og:image" content="https://rewards.rabbit.co.th/images/favicon/rabbit/mstile-144x144.png" />
        <link rel="icon" type="image/png" href="https://rewards.rabbit.co.th/images/favicon/rabbit/mstile-144x144.png" />
        <meta name="description" content="Earn 200 points just register or login rabbit rewards account.">
        <meta name="author" content="RabbitRewards Co., Ltd.">
        <script src="{{ asset('js/app.js') }}" defer></script>
        <title>Blaq x Rabbit</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="css/main.css" rel="stylesheet" media="all">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Kanit:300,300i,400,400i,500&amp;subset=thai" rel="stylesheet">
       <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Styles -->
        <script >
         $(document).ready(function() {
            //Checking Unique Email//
            var email = $("#checkEmail").val();
                       if(validateEmail(email)){
                  
                check_email_ajax(email);
                       
            }else if(email==""){
                $("#user-result").empty();
            }
            else{
                
                $("#user-result").html('Please fill real email.');
                document.getElementById("user-result").style.color = "red";
            }
             
        $("#checkEmail").change(function(){ 
             document.getElementById("button-sub").disabled = false;
            $("#user-result").html('<img src="img/ajax-loader.gif" class="img-loader">');
            var email = $("#checkEmail").val();
            
            if(validateEmail(email)){
                  
                check_email_ajax(email);
                       
            }else if(email==""){
                $("#user-result").empty();
            }
            else{
                
                $("#user-result").html('Please fill real email.');
                document.getElementById("user-result").style.color = "red";
            }
        }); 
        
            $("#checkPhone").change(function(){  
            $("#user-result2").html('<img src="img/ajax-loader.gif" class="img-loader">');
            var phone = $("#checkPhone").val();

            if(validatePhone(phone)){
               
                $("#user-result2").html('Can use this number.');
                document.getElementById("user-result2").style.color = "green";
            }else if(phone==""){
                $("#user-result2").empty();
            }
            else{
                $("#user-result2").html('<img src="img/ajax-loader.gif">');
                $("#user-result2").html('Fill real number for OTP.');
                document.getElementById("user-result2").style.color = "red";
            }

        }); 
            

            
           function validatePhone(full_phone) {
                
                if(full_phone.length != 10){
                    return false;
                }
                var two_phone = full_phone.substr(0,2);
                var two_valid = '';
                
                var two_1 = /08/;
                var two_2 = /09/;
                var two_3 = /06/;
                var phone = full_phone.substr(2,10);
               
                var phone_1 = /88888888/;
                var phone_2 = /11111111/;
                var phone_3 = /22222222/;
                var phone_4 = /33333333/;
                var phone_5 = /44444444/;
                var phone_6 = /55555555/;
                var phone_7 = /66666666/;
                var phone_8 = /77777777/;
                var phone_9 = /88888888/;
                var phone_10 = /99999999/;
                var phone_11 = /12345678/;
                var phone_12 = /87654321/;
                var phone_13 = /00001111/;
                var phone_14 = /90000000/;
               
                if(two_1.test(String(two_phone).toLowerCase())){
                    two_valid = 'A';
                }else if(two_2.test(String(two_phone).toLowerCase())){
                    two_valid = 'B';
                }else if(two_3.test(String(two_phone).toLowerCase())){
                    two_valid = 'C';
                }else{
                    return false;
                }
                
                if(two_valid != ''){
           
               
                if(phone_1.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_2.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_3.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_4.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_5.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_6.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_7.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_8.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_9.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_10.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_11.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_12.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_13.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_14.test(String(phone).toLowerCase())){
                    return false;
                }else{
                    return true;
                }
                       
                   }
            }

           function validateEmail(email) {
                
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var re_2 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(carrotrewards)+(.)+$/;
            var re_3 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(carrot)+(.)+$/;
            var re_4 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(c)+(.)+(wadr)+(.)+$/;
            var re_5 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(c)+(.)+(wrad)+(.)+$/;
            var re_6 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(carrct)+(.)+$/;
            var re_7 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(caootrews)+(.)+$/;   
            var re_8 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(crrot)+(.)+$/;
            var re_9 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(c)+(.)+(ward)+(.)+$/;
            var re_10 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(.)+(reward)+(.)+$/;
            var re_11 = /^(088)+(.)@(.)+(rabbit)+(.)+$/;  
            var re_12 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(noemail)+(.)+$/; 
            var re_13 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(carot)+(.)+$/;
            var re_14 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(getnada.com)+$/;
            
               
               
            if(re.test(String(email).toLowerCase())){
                if(re_14.test(String(email).toLowerCase())){
                      return false 
                }else if(re_2.test(String(email).toLowerCase())){
                      return false 
                }else if(re_3.test(String(email).toLowerCase())){
                      return false 
                }else if(re_4.test(String(email).toLowerCase())){
                      return false 
                }else if(re_5.test(String(email).toLowerCase())){
                      return false 
                }else if(re_6.test(String(email).toLowerCase())){
                      return false 
                }else if(re_7.test(String(email).toLowerCase())){
                      return false 
                }else if(re_8.test(String(email).toLowerCase())){
                      return false 
                }else if(re_9.test(String(email).toLowerCase())){
                      return false 
                }else if(re_10.test(String(email).toLowerCase())){
                      return false 
                }else if(re_11.test(String(email).toLowerCase())){
                      return false 
                }else if(re_12.test(String(email).toLowerCase())){
                      return false 
                }else if(re_13.test(String(email).toLowerCase())){
                      return false 
                }else{
                    return true;
                }
               
            }else{
                 return false;
              }
            
               
               
        }

        function check_email_ajax(email){
            $("#user-result").html('<img src="img/ajax-loader.gif" class="img-loader">');
  
             $.ajax({
                    type:"get",
                    url:"{{ url('/CheckEmail') }}",
                    data:
                        {email:email,
                        _token: '{{csrf_token()}}'},
           
                    success:function(data){

                        if(data==0){
                           document.getElementById("button-sub").disabled = false;
                            $("#user-result").html('Can use this email.');
                            document.getElementById("user-result").style.color = "green";

                        }

                        else{
                             
                            $("#user-result").html('This email is already registed.');
 
                           document.getElementById("user-result").style.color = "red";
                            
                        }
                    }
                 });

            }
 
           
});     
    
      function checkSubmit(){
//            alert("hi");
//        $.ajaxSetup({
//    headers: {
//        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//    }
//});
                   setTimeout(function() {
                      	
                    document.getElementById("button-sub").disabled = true;
    
    
                }, 100);
             
          
          $.when($.ajax(checkSubmiting())).then(function () {

            tester();

            });
       }
     
        function tester(){
                setTimeout(function() {
                      
                      document.getElementById("button-sub").disabled = false;
                }, 5500);
        }
        
        function checkSubmiting(){
             var x = document.getElementById("user-result").innerHTML;
             var y = document.getElementById("user-result2").innerHTML; 
                    if( x=='This email is already registed.' || x=='กรุณากรอกอีเมลล์ให้ถูกต้อง'){                 
                        document.getElementById("button-sub").disabled = true;
                        alert(x);
                        
                    }else if(y=='Fill real number for OTP.'){
                         document.getElementById("button-sub").disabled = true;
                        alert(y);
                    }
             
        }

    
</script>
        <script>
                function fb_login() {
                          
                         document.getElementById('min-h').style.height = '430px';
                          
                            
                      }

     function fb_register() {
     document.getElementById('min-h').style.height = '365px';
    }
           
            
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
            function click_item() {
                setTimeout(function() {
              
                      
                    document.getElementById("button-item1").disabled = true;
                    document.getElementById("button-item2").disabled = true;
                    document.getElementById("button-item3").disabled = true;
                    document.getElementById("button-item4").disabled = true;
                    document.getElementById("button-item5").disabled = true;
                    document.getElementById("button-item6").disabled = true;
                    document.getElementById("button-item7").disabled = true;
                    document.getElementById("button-item8").disabled = true;
                    document.getElementById("button-item9").disabled = true;
                    document.getElementById("button-item10").disabled = true;
                    document.getElementById("button-item11").disabled = true;
                    document.getElementById("button-item12").disabled = true;
              
                }, 100);
               
      
                
            }
            
                  
            function click_button() {
                setTimeout(function() {
                      	document.getElementById("button-d").disabled = true;
                    document.getElementById("button-d2").disabled = true;
                     document.getElementById("button-d3").disabled = true;
                    document.getElementById("button-sub").disabled = true;
    
    
                }, 100);
                  setTimeout(function() {
                      		document.getElementById("button-d").disabled = false;
                    document.getElementById("button-d2").disabled = false;
                       document.getElementById("button-d3").disabled = false;
                      document.getElementById("button-sub").disabled = false;
                }, 5500);
                 
            }
            
            function click_burn() {
                setTimeout(function() {
                    document.getElementById("button-item1").disabled = true;
                     document.getElementById("button-item2").disabled = true;
                     document.getElementById("button-item3").disabled = true;
              
              
                }, 100);
            }
            
         	$(function() {
        		$(".preload").fadeOut(400, function() {
        			$(".content").fadeIn(400);
        		});
        	});
            
</script>
<style>
.content {
	display:none;

}

.preload {
	margin:0;
	position:absolute;
	top:50%;
	left:50%;
	margin-right: -50%;
    max-width:150px; 
    width:20%;
	transform:translate(-50%, -50%);
}
</style>
    </head>
    <body>
    
        <div class="main-header">
            <div class="head-left">
                <img src="img/RR_tagline.png" onerror="this.onerror=null; this.src='img/RR_tagline.svg'" style="max-width: 120px; paddig-top:2px;">
            </div>
            <div class="head-right">
                <img src="img/rrlogo.png" onerror="this.onerror=null; this.src='img/rrlogo.svg'" style="max-width: 140px;">
            </div>
        <div style="clear:both;"></div>
        </div>
        <div class="preload" style="max-width:40%;margin:auto;">
            <img src="img/ajax-loader.gif" style="max-width:100%;margin:auto;">
        </div>
          <main id="myDiv" class="content">
            @yield('content')
        </main>
</body>
</html>