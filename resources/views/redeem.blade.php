@extends('layouts.app')

@section('content')
<div class="container" >
    
    
   
    @if(Auth::check())
<div class="login-wrap" style="text-align:center;">
<!--
 <a style="text-indent: 0px;line-height: 1.8;padding:10px;margin-bottom:50px;text-align:center;font-size:21px;color:#dc3545;margin:auto;font-weight: 500;">Congrats !!</a><br>
     <a style="text-indent: 0px;line-height: 1.2;padding:10px;margin-bottom:50px;text-align:center;font-size:21px;color:#dc3545;margin:auto;font-weight: 600;margin-bottom:10px;">You just earned 200 points.</a><br>
-->
   
    
@include('layouts.menu')
    
    <div class="alert alert-warning" role="alert">
        Hi, {{ Auth::user()->firstName }} Your balance is {{Auth::user()->wallet->balance}} points.
    </div>
      @if(Session::has('message'))
                  <p style="width:60%;margin:auto;" class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('message') }}</p>

        @endif


   <div style="margin-bottom:10px;">
<!--     <a style="text-indent: 0px;line-height: 1.8;padding:10px;text-align:center;font-size:22px;color:#fff;margin:auto;letter-spacing: 1.0px;">Buy-1-Get-1-Free</a><br>-->
</div>
   @foreach ($item as $i)
        @if($i->id == '1')
            <form action="{{url('/redeem')}}/{{$i->id}}">

                    <figure class="card card-product">

        <div class="row">
        <div class="col-6" style="text-align:center;margin:auto;padding-right:0px;">

           <div class="pic" >
                <img src="{{$i->item_path}}" class="item-pic" >
            </div>
        </div>
                <div class="col-6" style="text-align:center;margin:auto;padding:15px;padding-left:0px;">
                    <a style="font-size:20px;font-weight: 450;">Free !!</a><br>
                    <a style="font-size:19px;font-weight: 450;">{{$i->itemName}}</a><br>
                    <a style="font-size:13px;">{{$i->description}}</a><br>
                    <a style="font-size:14px;">Stock : {{$i->stock}}/{{$i->limit}}</a><br>
                    <div class="label-rating">{{$i->price}} points</div><br>

                    <button type="submit" id="button-item{{$i->id}}" onclick="click_item()" class="btn btn-sm btn-primary" style="background-color: #fd8204;border-color: #fd8204;border-radius:25px;width:60%;margin:8px;" value="Submit">Redeem</button>

        </div> <!-- col // -->
        </div>
                    </figure>

            </form>
        @else
		<form action="{{url('/redeem')}}/{{$i->id}}">
            
            <figure class="card card-product">
                
<div class="row">
<div class="col-6" style="text-align:center;margin:auto;padding-right:0px;">

   <div class="pic" >
		<img src="{{$i->item_path}}" class="item-pic" >
    </div>
</div>
        <div class="col-6" style="text-align:center;margin:auto;padding:15px;padding-left:0px;">
            <a style="font-size:20px;font-weight: 450;">Buy 1 Get 1</a><br>
            <a style="font-size:19px;font-weight: 450;">{{$i->itemName}}</a><br>
            <a style="font-size:13px;">{{$i->description}}</a><br>
            <a style="font-size:14px;">Stock : {{$i->stock}}/{{$i->limit}}</a><br>
            <div class="label-rating">{{$i->price}} points</div><br>
		
            <button type="submit" id="button-item{{$i->id}}" onclick="click_item()" class="btn btn-sm btn-primary" style="background-color: #fd8204;border-color: #fd8204;border-radius:25px;width:60%;margin:8px;" value="Submit">Redeem</button>

</div> <!-- col // -->
</div>
            </figure>
            
    </form>
    @endif
    @endforeach
    
    <div style="text-align:left; width:100%;color:white; font-size:small;">
        <b>Terms &amp; Conditions.</b>
        <ol style="padding-left: 20px;    padding-inline-start: 20px;text-align:left; ">
            <li>Each user is entitled to 200 points only.</li>
            <li>These points can be used in Bangkok Block Party on January 19-20, 2019 only. The remaining points are non-transferable to Rabbit Rewards points.
            </li>
           
        </ol>
    </div>
    
</div>
   @endif
    <div style="text-align:center;margin-top:10px;">
   <img src="img/ripndip-logo.png"  style="width:80%;margin:auto;max-width:600px;">
    </div>
</div>
 
@endsection