@extends('layouts.app')

@section('content')
<div class="container">
    <div class="login-wrap" style="min-height: 400px; padding:10px ;text-align:center;margin-bottom:10px;">
      <div class="row">
          <div class="twleve columns" style="text-align:center; padding-top:3%; color:white;margin:auto;width:100%">
              <h3 style="color:#ffffff; font-weight:400;"></h3>
              <div style="color:#fff; font-size:1.5em; line-height:1.2em;">Page not found</div><br>
              	<a href="{{ url('/')}}" class="btn btn-sm btn-primary" style="background-color: #fd8204;
    border-color: #fd8204;border-radius:25px;width:60%;margin:8px;" >Back to home page</a><br>
              <img src="img/rabbit_nottarget.png" class="u-max-full-width" style="min-width:80px; max-width: 60%;" />
          </div>
      </div>
        </div>
  </div>
@endsection
